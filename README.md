# netneighbours

This crate allows you to fetch entries from the system ARP and NDP tables.
It supports Linux, macOS and Windows (Vista and newer).

The Linux and macOS implementations parse the `ip neigh` and `ndp` plus `arp`
terminal commands output respectively.

The windows implementation however uses the WinAPI. This means unsafe code so
please take a look at the implementation.
