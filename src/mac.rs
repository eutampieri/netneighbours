use crate::{Table, run};

fn sanitize_mac(mac: &str) -> Option<macaddr::MacAddr6> {
    let mut mac = mac.split(':').flat_map(|x| u8::from_str_radix(x, 16).ok());
    Some(macaddr::MacAddr6::from([
        mac.next()?,
        mac.next()?,
        mac.next()?,
        mac.next()?,
        mac.next()?,
        mac.next()?,
    ]))
}

fn v6_parse(row: &str) -> Option<(std::net::IpAddr, macaddr::MacAddr6)> {
    let mut row = row.split(' ').filter(|x| x.len() > 0);
    let ip = row.next()?.split('%').next()?.parse().ok()?;
    let mac = sanitize_mac(row.next()?)?;
    Some((ip, mac))
}
fn v4_parse(row: &str) -> Option<(std::net::IpAddr, macaddr::MacAddr6)> {
    let mut row = row.split(' ').filter(|x| x.len() > 0).skip(1);
    let ip = row.next()?.replace('(', "").replace(')', "").parse().ok()?;
    let mac = sanitize_mac(row.skip(1).next()?)?;
    Some((ip, mac))
}

pub fn get_table() -> Table {
    run("ndp", "-anr")
        .split('\n')
        .skip(1)
        .filter_map(v6_parse)
        .chain(run("arp", "-an").split('\n').filter_map(v4_parse))
        .collect()
}
