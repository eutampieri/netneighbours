use winapi::shared::{
    netioapi::{FreeMibTable, GetIpNetTable2, MIB_IPNET_ROW2, PMIB_IPNET_TABLE2},
    nldef::{NlnsPermanent, NlnsReachable},
    ws2def::{AF_INET, AF_INET6},
};

use crate::Table;

fn parse_address(row: &MIB_IPNET_ROW2) -> Option<(std::net::IpAddr, macaddr::MacAddr6)> {
    let mac = if row.PhysicalAddressLength == 6 {
        Some(macaddr::MacAddr6::new(
            row.PhysicalAddress[0],
            row.PhysicalAddress[1],
            row.PhysicalAddress[2],
            row.PhysicalAddress[3],
            row.PhysicalAddress[4],
            row.PhysicalAddress[5],
        ))
    } else {
        None
    }?;
    let ip = match *unsafe { row.Address.si_family() } as i32 {
        AF_INET => {
            let bytes = unsafe { row.Address.Ipv4().sin_addr.S_un.S_un_b() };
            Some(std::net::IpAddr::V4(std::net::Ipv4Addr::new(
                bytes.s_b1, bytes.s_b2, bytes.s_b3, bytes.s_b4,
            )))
        }
        AF_INET6 => {
            let bytes = unsafe { row.Address.Ipv6().sin6_addr.u.Byte() };
            Some(std::net::IpAddr::V6(std::net::Ipv6Addr::from(*bytes)))
        }
        _ => None,
    }?;
    Some((ip, mac))
}

pub fn get_table() -> Table {
    let mut result = vec![];
    unsafe {
        let mut table: PMIB_IPNET_TABLE2 = std::mem::zeroed();
        if GetIpNetTable2(0, &mut table) == 0 {
            if !table.is_null() {
                let table_entries = &((*table).Table[0]) as *const MIB_IPNET_ROW2;
                for i in 0..(*table).NumEntries as isize {
                    let entry = *(table_entries.offset(i));
                    if let Some(entry) = parse_address(&entry) {
                        result.push(entry);
                    }
                }
            }
        }
        FreeMibTable(table as *mut winapi::ctypes::c_void);
    }
    result
}
