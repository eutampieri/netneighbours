#[cfg(target_os = "linux")]
mod linux;
#[cfg(target_os = "macos")]
mod mac;
#[cfg(target_os = "windows")]
mod windows;

type Table = Vec<(std::net::IpAddr, macaddr::MacAddr6)>;

#[cfg(target_os = "linux")]
pub use linux::get_table;
#[cfg(target_os = "macos")]
pub use mac::get_table;
#[cfg(target_os = "windows")]
pub use windows::get_table;

#[cfg(not(target_os = "windows"))]
fn run(cmd: &str, arg: &str) -> String {
    use std::process::Command;

    String::from_utf8_lossy(
        &Command::new(cmd)
            .arg(arg)
            .output()
            .expect("Failed to execute command")
            .stdout,
    )
    .to_string()
}

pub fn get_mac_to_ip_map() -> std::collections::HashMap<macaddr::MacAddr6, Vec<std::net::IpAddr>> {
    get_table()
        .into_iter()
        .fold(Default::default(), |mut acc, (ip, mac)| {
            if !acc.contains_key(&mac) {
                acc.insert(mac, vec![]);
            }
            let list = acc.get_mut(&mac).unwrap();
            if !list.contains(&ip) {
                list.push(ip);
            }
            acc
        })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        dbg!(get_mac_to_ip_map());
        for (ip, mac) in get_table() {
            println!("IP: {ip} -> {mac}");
        }
    }
}
