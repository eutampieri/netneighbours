use crate::{run, Table};

fn parse(row: &str) -> Option<(std::net::IpAddr, macaddr::MacAddr6)> {
    let mut row = row.split(' ');
    let ip = row.next()?.parse().ok()?;
    let mac = row.skip(3).next()?.parse().ok()?;
    Some((ip, mac))
}

pub fn get_table() -> Table {
    run("ip", "neigh")
        .split("\n")
        .filter(|x| x.len() > 0)
        .filter_map(parse)
        .collect()
}
